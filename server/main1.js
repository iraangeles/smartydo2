// SMARTYDO project
//
databaseTask
var express = require('express');
var path = require("path");
var app = express();

var databaseTask = [];
var taskObject = {};

databaseTask = [        {"id": "1","name": "Shop ingredients for Kaya","taskdate": "10/02/17","description":"Eggs, Sugar, pandan leaves...","priority":"low","category":"family","status":"Pending","notify":"no"},
                        {"id": "2","name": "Submit report to Mr Go ","taskdate": "14/03/17","description":"","priority":"low","category":"work","status":"Completed","notify":"no"}, 
                        {"id": "3","name": "Come up with a list of requirements","taskdate": "20/02/17","description":"discuss with Elissa nad the team first","priority":"medium","category":"work","status":"Pending","notify":"no"}, 
                        {"id": "4","name": "Bring Cooper to the park","taskdate": "17/02/17","description":"","priority":"low","category":"familu","status":"Pending","notify":"no"}, 
                        {"id": "5","name": "Learn to cook kaya","taskdate": "25/02/17","description":"","priority":"low","category":"family","status":"Pending","notify":"no"}, 
                        {"id": "6","name": "Have tea with the committee","taskdate": "24/02/17","description":"call members to decide on the date","priority":"low","category":"social","status":"Pending","notify":"no"},   
                    ];

var createTask = function(tmp){
    return ({
        id: tmp.taskid,
        name : tmp.name,
        taskdate : tmp.taskdate,
        description : tmp.description,
        priority : tmp.priority,
        category : tmp.category,
        notify : tmp.notify,
        status : tmp.status
    });
};
 


// Configure port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000);

//Set bower route
app.use("/libs",express.static(path.join(__dirname,"bower_components")));

app.get("/getalltask", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(databaseTask);
});

app.get("/addtask", function(req, resp){
    console.log("Adding task", JSON.stringify(req.query));
    databaseTask.push(createTask(req.query));
    console.log("All orders:\n %s", JSON.stringify(databaseTask));
    
	resp.status(201).end();
});

app.get("/updatetask", function(req, resp){
    console.log("Updating task", JSON.stringify(req.query));
   var updatedtask = createTask(req.query);
   var idx = databaseTask.indexOf(updatedtask.taskid);
   databaseTask[idx] = updatedtask;
   console.log("All orders:\n %s", JSON.stringify(databaseTask));
   
    resp.status(201).end();
});

//Configure routes
app.use(express.static(path.join(__dirname,"public")));

app.listen(app.get("port"),function(){
    console.log("SMARTYDO Application started at %s on port %d", new Date(),app.get("port"));

}
);
