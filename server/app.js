// SMARTYDO project
//

var express = require('express');
var path = require("path");
var app = express();

var databaseTask = [];
var taskObject = {};

// "id": "1f","  change from "1" to "1f" since "1"  is not unique

databaseTask = [    {"id": "1f","name": "Shop ingredients for Kaya","taskdate": "10/02/2017","description":"Eggs, Sugar, pandan leaves...","priority":"low","category":"family","status":"pending","notify":"no"},
                        {"id": "1a","name": "Submit report to Mr Go ","taskdate": "14/03/2017","description":"","priority":"low","category":"work","status":"completed","notify":"no"}, 
                        {"id": "1b","name": "Come up with a list of requirements","taskdate": "21/02/2017","description":"discuss with Elissa nad the team first","priority":"medium","category":"work","status":"pending","notify":"no"}, 
                        {"id": "1c","name": "Bring Cooper to the park","taskdate": "17/02/2017","description":"","priority":"low","category":"family","status":"pending","notify":"no"}, 
                        {"id": "1d","name": "Learn to cook kaya","taskdate": "25/02/2017","description":"","priority":"low","category":"family","status":"pending","notify":"no"}, 
                        {"id": "1e","name": "Have tea with the committee","taskdate": "24/02/2017","description":"call members to decide on the date","priority":"low","category":"social","status":"pending","notify":"no"},   
                    ];

category = ["work","social","family","friends","none"];

var createTask = function(tmp){
    console.log('tmp.taskdate=======================', tmp, tmp.taskdate.slice(0,10));
    return ({
        id: tmp.id,  // updated this property
        name : tmp.name,
        taskdate : tmp.taskdate.slice(0,10),
        description : tmp.description,
        priority : tmp.priority,
        category : tmp.category,
        notify : tmp.notify,
        status : tmp.status
    });
};

// function go get index 
var  findIndexInData = function (data, property, value) {
  for(var i = 0, l = data.length ; i < l ; i++) {
    if(data[i][property] === value) {
      return i;
    };
  };
  return -1;
};

// console.log (databaseTask.find( { id : '1f' }));

// Configure port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000);

//Set bower route
// app.use("/libs",express.static(path.join(__dirname,"client/bower_components")));

app.get("/getalltask", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
    console.log("Array of objects returned.....", JSON.stringify(databaseTask));
    resp.json(databaseTask);

});

app.get("/getcategory", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(category);
});


app.get("/addtask", function(req, resp){
    console.log("Adding task", JSON.stringify(req.query));
    databaseTask.push(createTask(req.query));
    console.log("All orders:\n %s", JSON.stringify(databaseTask));
    console.log("any string......");
	resp.status(201).json(databaseTask);
});

app.get("/updatetask", function(req, resp){
   var updatedtask = createTask(req.query);
    var idx = findIndexInData(databaseTask , 'id', updatedtask.id );

   databaseTask.splice(idx,1,updatedtask);  // using splice to update the element in the database array
   console.log("Updated this task in server:\n %s\n", JSON.stringify(databaseTask[idx]));

   console.log("All task in server:\n %s\n", JSON.stringify(databaseTask));   

    resp.status(201).end();
});

app.get("/updatecategory", function(req, resp){
  var updatedtask = createTask(req.query);

   category = req.query;

   console.log("All category in server:\n %s\n", JSON.stringify(category));  

   resp.status(201).end();

});

//Configure routes
app.use(express.static(path.join(__dirname,"/../client")));

app.listen(app.get("port"),function(){
    console.log("SMARTYDO Application started at %s on port %d", new Date(),app.get("port"));

}
);