### APP REQUIREMENTS 

#### Base ####

- [x] User can create a task by providing the following information:
    * Task title (mandatory)
    * Task description 
    * Task priority — low, medium, high (default to medium)
    * Task category —  categories: social, family, work (default to none)
    * Due date (default to this week)
    * Reminder days before due — default to one day
- [x] User can mark a task as complete
- [x] User can delete a pending task 
- [x] User can filter the lists in one or all categories
- [x] User can list all overdue tasks
- [x] User can list all tasks due today
- [x] User can list all tasks due next 7 days
- [x] User can list all pending tasks
- [x] User can list all completed tasks 
- [x] User can sort all lists by due dates
- [ ] User can update task details 
- [ ] User can add a category 
- [ ] User can delete a category if not assigned to any tasks 


#### Additional ####
- [ ] User can “revive” tasks
- [ ] User can archive completed tasks 
- [ ] User can bulk update overdue tasks 
- [ ] User can automatically reassign due dates of overdue tasks based on task priorities - high today, med this week, low next week 
- [ ] User can edit a category
- [ ] User can get a reminder X days before task is due